#!/bin/sh

docker_with_login() {
	if [ ! -e ~/.docker/config.json ]; then
		command docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY || fail "Unable to login with docker"
	fi
	command docker "$@"
}

docker_build_and_push() {
	docker_with_login build --pull -t $CI_REGISTRY_IMAGE:$1 .
	docker tag $CI_REGISTRY_IMAGE:$1 $CI_REGISTRY_IMAGE:$2  || fail "Unable to tag image"
	docker_with_login push $CI_REGISTRY_IMAGE:$2
}
